<?php $__env->startSection('content'); ?>
<style>
#myform {
    text-align: center;
    padding: 5px;
    border: 1px dotted #ccc;
    margin: 2%;
}
.qty {
    width: 40px;
    height: 25px;
    text-align: center;
}
input.qtyplus { width:25px; height:25px;}
input.qtyminus { width:25px; height:25px;}
</style>
<div class="container">
		<div id="content">
			
			<div class="table-responsive">
				<!-- Shop Products Table -->
				<table class="shop_table beta-shopping-cart-table" cellspacing="0">
					<thead>
						<tr>
							<th class="product-name">Sản phẩm</th>
							<th class="product-price">Đơn giá</th>
							<!-- <th class="product-status"></th> -->
							<th class="product-quantity">Số lượng</th>
							<th class="product-subtotal">Số tiền</th>
							<th class="product-remove">Thao tác</th>
						</tr>
					</thead>
					<tbody>
					<?php if(Session::has('cart')): ?>
								<?php $__currentLoopData = $product_cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr class="cart_item">
						
							<td class="product-name">
								<div class="media">
									<img class="pull-left"width="25%" src="source/image/product/<?php echo e($cart['item']['image']); ?>" alt="">
									<div class="media-body">
										<p class="font-large table-title"><?php echo e($cart['item']['name']); ?></p>
										<!-- <p class="table-option">Color: Red</p>
										<p class="table-option">Size: M</p>
										<a class="table-edit" href="#">Edit</a> -->
									</div>
								</div>
							</td>

							<td class="product-price">
								<span class="amount"><?php echo e(number_format($cart['item']['unit_price'])); ?>đ</span>
							</td>

							<!-- <td class="product-status">
								In Stock
							</td> -->

							<td class="product-quantity ">
							
							<!-- <?php echo e($cart['qty']); ?> -->
							<input type='button' value='-' class='qtyminus' field='quantity' />
    <input type='text' name='quantity' value='0' class='qty' />
    <input type='button' value='+' class='qtyplus' field='quantity' />
							</td>

							<td class="product-subtotal">
								<span class="amount"><?php echo e(number_format($cart['price'])); ?>đ</span>
							</td>

							<td class="product-remove">
								<a href="<?php echo e(route('xoagiohang',$cart['item']['id'])); ?>" class="remove" title="Remove this item"><i class="fa fa-trash-o"></i></a>
							</td>
							
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
					</tbody>

					<!-- <tfoot>
						<tr>
							<td colspan="6" class="actions">

								<div class="coupon">
									<label for="coupon_code">Coupon</label> 
									<input type="text" name="coupon_code" value="" placeholder="Coupon code"> 
									<button type="submit" class="beta-btn primary" name="apply_coupon">Apply Coupon <i class="fa fa-chevron-right"></i></button>
								</div>
								
								<button type="submit" class="beta-btn primary" name="update_cart">Update Cart <i class="fa fa-chevron-right"></i></button> 
								<button type="submit" class="beta-btn primary" name="proceed">Proceed to Checkout <i class="fa fa-chevron-right"></i></button>
							</td>
						</tr>
					</tfoot> -->
				</table>
				<!-- End of Shop Table Products -->
			</div>


			<!-- Cart Collaterals -->
			<div class="cart-collaterals">

				<div class="cart-totals pull-right">
					<div class="cart-totals-row"><h5 class="cart-total-title">Cart Totals</h5></div>
					<div class="cart-totals-row"><span>Cart Subtotal:</span> <span>$188.00</span></div>
					<div class="cart-totals-row"><span>Shipping:</span> <span>Free Shipping</span></div>
					<div class="cart-totals-row"><span>Order Total:</span> <span>$188.00</span></div>

					<div class="cart-totals-row"style="background-color:orange; text-align:center"><a href="<?php echo e(route('dathang')); ?>" style="color:white">Đặt Hàng</a></div>
				</div>

				
				<div class="clearfix"></div>
				
			</div>
			<!-- End of Cart Collaterals -->
			<div class="clearfix"></div>

		</div> <!-- #content -->
	</div> <!-- .container -->

	<script>
jQuery(document).ready(function(){
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
});
	</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>