<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
class ProductController extends Controller
{
    public function getList(){
        $products = Product::where('isShow', 1)->get();
        return view('admin.product.list',['products'=>$products]);
    }
    public function getAdd(){
        return view('admin.product.add');
    }
    public function getDelete($id){
        DB::table('products')
            ->where('id', $id)
            ->update(['isShow' => false]);

        return redirect('admin/product/list')->with('thongbao','Xóa thành công');
    }
}
